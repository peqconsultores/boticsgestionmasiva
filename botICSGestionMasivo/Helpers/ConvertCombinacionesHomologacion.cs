﻿using botICSGestionMasivo.Constantes;
using botICSGestionMasivo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Helpers
{
    public static class ConvertCombinacionesHomologacion
    {
        public static ResponseCombinacionHomologacion obtenerCombinacion(string canalContacto, string estadoContacto, string estadoCliente, string guion1, string guion2, string guion3, string mensaje)
        {
            ResponseCombinacionHomologacion response = new ResponseCombinacionHomologacion()
            {
                codigo = 1,
                mensaje = "Combinación con respuesta encontrada."
            };

            if (canalContacto == ConstanteCanalContacto.LLAMADA_OUT)
            {
                if (estadoContacto == ConstanteEstadoContacto.CONTACTO_DIRECTO)
                {
                    if (estadoCliente == ConstanteEstadoCliente.COMPROMISO_DE_PAGO)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.COMPROMISO)
                            {
                                if (guion3 == ConstanteGuion3.PLAN_ALIVIO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.PROMISE;
                                    response.columnaM = mensaje;
                                    return response;

                                }
                                else if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.RECORDANDO_PAGO;
                                    response.columnaM = mensaje;
                                    return response;

                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.COMPROMISO_VIGENTE)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.RECORDANDO_PAGO;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.RECORDANDO_PROMESA)
                            {
                                if (guion3 == ConstanteGuion3.RECORDANDO_PLAN_ALIVIO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.RECORDANDO_PAGO;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else if (estadoCliente == ConstanteEstadoCliente.RENUENTE)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.DESACUERDO)
                            {
                                if (guion3 == ConstanteGuion3.NO_ESTA_DEACUERDO_CON_EL_VALOR_COBRADO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.ALTERNATIVA;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            if (guion2 == ConstanteGuion2.RENUENTE)
                            {
                                if (guion3 == ConstanteGuion3.SIN_INTERES_DE_PAGO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.RENUENTE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else if (estadoCliente == ConstanteEstadoCliente.INSOLVENTE)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.DIFICULTAD_DE_PAGO)
                            {
                                if (guion3 == ConstanteGuion3.DESEMPLEO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.ALTERNATIVA;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else if (estadoCliente == ConstanteEstadoCliente.SEGUIMIENTO_Y_O_PRESION)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.ESTADO_ICS)
                            {
                                if (guion3 == ConstanteGuion3.ESTADO_INACTIVO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.ALTERNATIVA;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            if (guion2 == ConstanteGuion2.SIN_COMPROMISO_DE_PAGO)
                            {
                                if (guion3 == ConstanteGuion3.PRESENTA_RECLAMO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.ALTERNATIVA;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            if (guion2 == ConstanteGuion2.VOLUNTAD_DE_PAGO)
                            {
                                if (guion3 == ConstanteGuion3.PAGO_POSTERIOR)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.ALTERNATIVA;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else if (estadoCliente == ConstanteEstadoCliente.POSIBLE_ACUERDO)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.OFRECIMIENTO_DE_CONDONACION)
                            {
                                if (guion3 == ConstanteGuion3.POSIBLE_CONDONACION)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.ALTERNATIVA;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else if (estadoCliente == ConstanteEstadoCliente.YA_PAGO)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.PAGO_EN_APLICATIVO)
                            {
                                if (guion3 == ConstanteGuion3.SE_EVIDENCIA_PAGO_EN_APLICATIVO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.YA_PAGO;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.YA_PAGO)
                            {
                                if (guion3 == ConstanteGuion3.CLIENTE_AL_DIA)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.YA_PAGO;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else if (estadoCliente == ConstanteEstadoCliente.VOLVER_A_LLAMAR)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_DIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.VOLVER_A_LLAMAR)
                            {
                                if (guion3 == ConstanteGuion3.VOLVER_A_LLAMAR)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.YA_PAGO;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else
                    {
                        response.codigo = 0;
                        response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                    }
                }
                else if (estadoContacto == ConstanteEstadoContacto.CONTACTO_INDIRECTO)
                {
                    if (estadoCliente == ConstanteEstadoCliente.MENSAJE_CON_TERCERO)
                    {
                        if (guion1 == ConstanteGuion1.CONTACTO_INDIRECTO)
                        {
                            if (guion2 == ConstanteGuion2.APORTAN_INFORMACION)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.MESSAGE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if(guion2 == ConstanteGuion2.FALLECIDA)
                            {
                                if (guion3 == ConstanteGuion3.FALLECIDO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.FALLECIDO;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.NO_SE_DEJA_MENSAJE)
                            {
                                if (guion3 == ConstanteGuion3.NO_SE_DEJA_MASAJE)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.MESSAGE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.SE_DEJA_MENSAJE)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.MESSAGE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.SE_DEJA_MENSAJE_CON_FAMILIAR)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.MESSAGE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.SE_DEJA_MENSAJE_EN_EL_TRABAJO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.MESSAGE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.SE_DEJA_MENSAJE_OTRO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.MESSAGE;
                                    response.columnaM = mensaje;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else
                    {
                        response.codigo = 0;
                        response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                    }
                }
                else if (estadoContacto == ConstanteEstadoContacto.SIN_CONTACTO)
                {
                    if (estadoCliente == ConstanteEstadoCliente.SIN_CONTACTO)
                    {
                        if (guion1 == ConstanteGuion1.SIN_CONTACTO)
                        {
                            if (guion2 == ConstanteGuion2.COLGO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if(guion2 == ConstanteGuion2.CONTESTA_PERO_COLGO)
                            {
                                if (guion3 == ConstanteGuion3.CONTESTA_Y_COLGO)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.FUERA_DE_SERVICIO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.MENSAJE_BUZON)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.MOVIL_APAGADO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.NO_CONOCIDO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.NO_CONTESTA)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.NUMERO_ERRADO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.OCUPADO)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else if (guion2 == ConstanteGuion2.SE_ENVIA_SOLICITUD)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else
                    {
                        response.codigo = 0;
                        response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                    }
                }
                else
                {
                    response.codigo = 0;
                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                }
            }
            if (canalContacto == ConstanteCanalContacto.LLAMADA_IN)
            {
                if (estadoContacto == ConstanteEstadoContacto.SIN_CONTACTO)
                {
                    if (estadoCliente == ConstanteEstadoCliente.SIN_CONTACTO)
                    {
                        if (guion1 == ConstanteGuion1.EMPTY)
                        {
                            if (guion2 == ConstanteGuion2.EMPTY)
                            {
                                if (guion3 == ConstanteGuion3.EMPTY)
                                {
                                    response.columnaJ = ConstanteRespuestaColumnaJ.MARCADOR;
                                    response.columnaK = ConstanteRespuestaColumnaK.NO_ANSWER;
                                    response.columnaM = ConstanteRespuestaColumnaM.NO_CONTESTA;
                                    return response;
                                }
                                else
                                {
                                    response.codigo = 0;
                                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                                }
                            }
                            else
                            {
                                response.codigo = 0;
                                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                            }
                        }
                        else
                        {
                            response.codigo = 0;
                            response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                        }
                    }
                    else
                    {
                        response.codigo = 0;
                        response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                    }
                }
                else
                {
                    response.codigo = 0;
                    response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
                }
            }
            else
            {
                response.codigo = 0;
                response.mensaje = "No se encontró respuestas en la homologación para la combinación ingresada.";
            }
            return response;
        }
    }
}
