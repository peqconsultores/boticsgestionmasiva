﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Diagnostics;
using log4net;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using botICSGestionMasivo.Entities;
using System.Collections.Generic;
using botICSGestionMasivo.Helpers;
using System.Linq;
using OpenQA.Selenium.Chrome;

namespace botICSGestionMasivo.Proceso
{
    class SubidaExcel
    {
        private static ILog log = LogManager.GetLogger(typeof(DescargaTxt));

        private static string rutaHomologacion = ConfigurationManager.AppSettings["rutaHomologacionArchivoTxt"];
        private static string rutaSubido = ConfigurationManager.AppSettings["rutaSubidaArchivosExcelCsv"];
        private static string rutaNOSubido = ConfigurationManager.AppSettings["rutaNOSubidaArchivosExcelCsv"];

        private string urlPageLogin = ConfigurationManager.AppSettings["iCS_url_login"];
        private string urlPageMain = ConfigurationManager.AppSettings["iCS_url_main"];
        private string urlPageAdminSubidaArchivo = ConfigurationManager.AppSettings["iCS_url_admin_subida_archivo"];
        private string urlPageError = ConfigurationManager.AppSettings["iCS_url_error"];
        private string urlPageExito = ConfigurationManager.AppSettings["iCS_url_exito"];
        private string userICS = ConfigurationManager.AppSettings["iCS_user"];
        private string passwordICS = ConfigurationManager.AppSettings["iCS_password"];

        protected By usuario = By.Id("j_id_f:username");
        protected By contrasenia = By.Id("fscpassword_j_id_f:password");
        protected By inicioSesion = By.Id("j_id_f:BtnLogin");
        protected By seleccionarArchivo = By.Name("fileList");
        protected By envioArchivo = By.XPath("//input[@type='submit' and @value='Envío']");

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void keybd_event(uint bVk, uint bScan, uint dwFlags, uint dwExtraInfo);
        protected IWebDriver Driver;
        public void subidaICS()
        {
            log.Info("Se inicia el navegador. Fecha: " + DateTime.Now.ToString());
            ChromeOptions options = new ChromeOptions();
            Driver = new ChromeDriver(options);
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 1, 0));

            log.Info("Se inicia la busqueda de archivos homologados. Fecha: " + DateTime.Now.ToString());
            string[] files = Directory.GetFiles(rutaHomologacion, "*.txt", SearchOption.TopDirectoryOnly);

            if (files != null && files.Length > 0)
            {
                Driver.Navigate().GoToUrl(urlPageLogin);
                Driver.Manage().Window.Maximize();
                wait.Until(ExpectedConditions.ElementExists(usuario));
                Driver.FindElement(usuario).SendKeys(userICS);
                wait.Until(ExpectedConditions.ElementExists(contrasenia));
                Driver.FindElement(contrasenia).SendKeys(passwordICS);
                wait.Until(ExpectedConditions.ElementExists(inicioSesion));
                wait.Until(ExpectedConditions.ElementToBeClickable(inicioSesion));
                Driver.FindElement(inicioSesion).Click();

                do
                {
                    log.Info("A la espera de que se inicie sesión. Fecha: " + DateTime.Now.ToString());
                    Thread.Sleep(5000);
                }
                while (Driver.Url != urlPageMain);

                wait.Until(ExpectedConditions.UrlMatches(urlPageMain));
                log.Info("Se detectó el inicio de sesión. Fecha: " + DateTime.Now.ToString());
                Thread.Sleep(1000);


                foreach(var file in files)
                {
                    //cambiar
                    Driver.Navigate().GoToUrl(urlPageAdminSubidaArchivo);
                    Thread.Sleep(500);
                    Driver.FindElement(seleccionarArchivo).SendKeys(file);
                    Thread.Sleep(500);
                    Driver.FindElement(envioArchivo).Click();

                    Thread.Sleep(5000);

                    do
                    {
                        Thread.Sleep(10000);
                    }
                    while (Driver.Url== urlPageAdminSubidaArchivo);

                    if (Driver.Url == urlPageExito)
                    {
                        try
                        {
                            File.Move(file, rutaSubido + "\\" + Path.GetFileName(file));
                        }
                        catch (Exception e)
                        {
                            log.Error("Ocurrio un error al mover un archivo csv."+ Path.GetFileName(file) + " Detalle: " + e.Message);
                            File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                        }
                    }
                    else 
                    {

                        try
                        {
                            File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                        }
                        catch (Exception e)
                        {
                            log.Error("Ocurrio un error al mover un archivo csv." + Path.GetFileName(file) + "Detalle: " + e.Message);
                            File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                        }
                    }
                }
                Driver.Close();
                Driver.Dispose();
            }
            else
            {
                Driver.Close();
                Driver.Dispose();
                log.Error("No se pudo encontrar archivos csv en la carpeta " + rutaHomologacion + " . Fecha: " + DateTime.Now.ToString());
            }            
        }
    }
}
