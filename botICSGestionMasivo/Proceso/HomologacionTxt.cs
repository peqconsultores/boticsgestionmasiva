﻿using botICSGestionMasivo.Entities;
using botICSGestionMasivo.Helpers;
using log4net;
using Microsoft.VisualBasic.FileIO;
using NUnit.Framework;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Proceso
{
    class HomologacionTxt
    {
        private static ILog log = LogManager.GetLogger(typeof(DescargaTxt));
        private string sourcefile1 = ConfigurationManager.AppSettings["rutaDescargaArchivoTxt"]+ "\\Gestión Universo-SERFINANZA PRE-JURIDICO-SERFINANZA PRE JURIDICO " + DateTime.Now.Year+"-" + DateTime.Now.ToString("MM") + "-" + DateTime.Now.ToString("dd")+".txt";
        private string rutaArchivos = ConfigurationManager.AppSettings["rutaArchivoTxt"];
        private string rutaHomologado = ConfigurationManager.AppSettings["rutaHomologacionArchivoTxt"];
        private string rutaNoHomologado = ConfigurationManager.AppSettings["rutaNOHomologacionArchivoTxt"];
        private static string rutaSubido = ConfigurationManager.AppSettings["rutaSubidaArchivosExcelCsv"];
        private static string rutaNOSubido = ConfigurationManager.AppSettings["rutaNOSubidaArchivosExcelCsv"];
        public ResponseSimple ConvertToCsv()
        {
            ResponseSimple response = new ResponseSimple()
            {
                codigo = 0,
                mensaje = "No se encontraron los archivos"
            };

            if (File.Exists(sourcefile1))
            {
                log.Info("Se inicia el navegador. Fecha: " + DateTime.Now.ToString());
                try
                {
                    int i, j;
                    StreamWriter csvfile;
                    string[] lines, cells;
                    lines = File.ReadAllLines(sourcefile1);
                    csvfile = new StreamWriter(rutaArchivos + "\\serfinanza.csv");
                    for (i = 0; i < lines.Length; i++)
                    {
                        cells = lines[i].Split(new Char[] { '\t', ';' });
                        for (j = 0; j < cells.Length; j++)
                            //csvfile.Write(cells[j] + ",");
                            csvfile.Write(cells[j] + ";");
                        csvfile.WriteLine();
                    }
                    csvfile.Close();
                    response.codigo = 1;
                    response.mensaje = "Se convertido el archivo a formato csv.";
                    response.valor = rutaArchivos + "\\serfinanza.csv";
                    try
                    {
                        File.Delete(sourcefile1);
                    }
                    catch (Exception e)
                    {
                        log.Error("El archivo: " + sourcefile1 + " no pudo ser eliminado de la carpeta de descargas (Se recomienda eliminación manual) . Fecha: " + DateTime.Now.ToString());
                    }
                }
                catch(Exception e)
                {
                    response.codigo = -1;
                    response.mensaje = "Ocurrió un error al intenar convertir el archivo de txt a csv";
                    log.Error("El archivo: " + sourcefile1 + " no pudo convertirse a .csv . Fecha: " + DateTime.Now.ToString());
                }
            }
            else
            {                
                log.Error("El archivo: "+sourcefile1+" no existe . Fecha: " + DateTime.Now.ToString());
            }
            return response;
        }
        public ResponseSimple homologarRegistros()
        {
            log.Info("Inicio homologacion de archivos xlsx por entidad. Fecha: " + DateTime.Now.ToString());
            ResponseSimple response = new ResponseSimple()
            {
                codigo = 0,
                mensaje = "No se pudo homologar los registros."
            };
            var combinacion = new ResponseCombinacionHomologacion();

            string[] files = Directory.GetFiles(rutaArchivos, "*.csv", System.IO.SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var datosExcel = obtenerDatos(file);
                if (datosExcel.codigo == 1)
                {
                    if (datosExcel.totalRows > 0)
                    {
                        int contadorRow = 0;
                        int contadorArchivos = 0;
                        if (contadorRow == 0)
                        {
                            log.Info("Creando archivo excel. N° (" + (contadorArchivos + 1) + ") Fecha: " + DateTime.Now.ToString());
                        }

                        /* Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

                         if (xlApp == null)
                         {
                             log.Info("Excel is not properly installed!!. Fecha: " + DateTime.Now.ToString());
                             return new ResponseSimple()
                             {
                                 codigo = -1,
                                 mensaje = "Excel is not properly installed!!."
                             };
                         }
                         Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                         Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                         object misValue = System.Reflection.Missing.Value;

                         xlWorkBook = xlApp.Workbooks.Add(misValue);
                         xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);*/

                        string rutaCompleta = rutaHomologado+ "\\" + Path.GetFileName(file).Replace(".csv","")+(contadorArchivos+1)+".txt";
                        using (StreamWriter mylogs = File.AppendText(rutaCompleta))         //se crea el archivo
                        {
                            //DateTime dateTime = new DateTime();
                            //dateTime = DateTime.Now;
                            //string strDate = Convert.ToDateTime(dateTime).ToString("yyMMdd");

                            foreach (var row in datosExcel.rows)
                            {
                                try
                                {
                                    combinacion = ConvertCombinacionesHomologacion.obtenerCombinacion(row.canalContacto, row.estadoContacto, row.estadoCliente, row.guion1, row.guion2, row.guion3, row.observacion);
                                    if (combinacion.codigo != 0)
                                    {

                                        mylogs.WriteLine(row.fecha.Replace("-","/") + " " + row.hora + "	" +
                                            row.numeroDocumento + "	" +
                                           "SERFINANSA" + "	" +
                                            "	" +
                                           "CARTERA OLIMPICA" + "	" +
                                            "	" +
                                           "1" + "	" +
                                           "EXT01GF41" + "	" +
                                           "N" + "	" +
                                           combinacion.columnaJ + "	" +
                                           combinacion.columnaK + "	" +
                                            "	" +
                                           combinacion.columnaM + "	" +
                                            "	" +
                                           row.telefono + "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                            "	" +
                                           "GZOLIMPICA" + "	" +
                                           "3" + "	" +
                                            "	" +
                                           "PENDING");

                                        contadorRow++;

                                        log.Info("Se añadió la linea : " +
                                            row.fecha + " " + row.hora + "	" +
                                            row.numeroObligacion + "	" +
                                            "SERFINANSA" + "	" +
                                            "" + "	" +
                                            "CARTERA OLIMPICA" + "	" +
                                            "" + "	" +
                                            "1" + "	" +
                                            "EXT01GF41" + "	" +
                                            "N" + "	" +
                                            combinacion.columnaJ + "	" +
                                            combinacion.columnaK + "	" +
                                            "" + "	" +
                                            combinacion.columnaM + "	" +
                                            "" + "	" +
                                            row.numeroDocumento + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "" + "	" +
                                            "GZOLIMPICA" + "	" +
                                            "3" + "	" +
                                            "" + "	" +
                                            "PENDING");
                                    }
                                }
                                catch (Exception e)
                                {
                                    log.Error("Hubo un error al intentar insertar un registro. Fecha: " + DateTime.Now.ToString());
                                }
                            }
                            mylogs.Close();
                        }

                        /*
                        if (File.Exists(rutaHomologado + "\\" + Path.GetFileName(file).Replace(".csv", (contadorArchivos + 1).ToString())+ ".csv"))
                        {
                            xlWorkBook.Save();
                            log.Info("Se guardó el archivo de " + Path.GetFileName(file).Replace(".csv", (contadorArchivos + 1).ToString()) + ". Fecha: " + DateTime.Now.ToString());
                        }
                        else
                        {
                            xlWorkBook.SaveAs(rutaHomologado + "\\" + Path.GetFileName(file).Replace(".csv", (contadorArchivos + 1).ToString()), Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                            log.Info("Se guardó el archivo de " + Path.GetFileName(file).Replace(".csv", (contadorArchivos + 1).ToString()) + ". Fecha: " + DateTime.Now.ToString());
                        }
                        xlWorkBook.Close(true, misValue, misValue);
                        xlApp.Quit();

                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);*/

                        if (contadorRow == 9000)
                        {
                            contadorRow = 0;
                            contadorArchivos++;
                        }
                        log.Info("El archivo excel para la entidad fue creado. Fecha: " + DateTime.Now.ToString());
                        response.codigo = 1;
                        response.mensaje = "Se ha creado al menos 1 archivo";
                         
                    }
                    else
                    {
                        response.codigo = 0;
                        response.mensaje = "El archivo no contiene suficientes datos para realizar la homologacion.";
                        log.Error("El excel no contiene suficientes datos para registrar.");
                        File.Move(file, rutaNoHomologado + "\\" + Path.GetFileName(file));
                    }
                }
                else
                {
                    response.codigo =-1;
                    response.mensaje = "No se pudo leer el archivo para realizar la homologacion.";
                    log.Error("No se pudo leer el excel: Mensaje: " + datosExcel.mensaje + "Fecha: " + DateTime.Now.ToString());
                    if (File.Exists(file))
                    {
                        File.Move(file, rutaNoHomologado + "\\" + Path.GetFileName(file));
                    }
                }

                try
                {
                    if (File.Exists(file))
                    {
                        File.Move(file, rutaSubido + "\\" + Path.GetFileName(file));
                    }
                }
                catch (Exception e)
                {
                    log.Error("Ocurrio un error al mover un archivo csv. Detalle: " + e.Message); if (File.Exists(file))
                    {
                        if (File.Exists(file))
                        {
                            File.Move(file, rutaNOSubido + "\\" + Path.GetFileName(file));
                        }
                    }
                }
            }

            log.Info("Finalizo homologacion de archivos xlsx por entidad. Fecha: " + DateTime.Now.ToString());
            return response;
        }
        public static DataTxtResponse obtenerDatos(string pathFile)
        {
            DataTxtResponse response = new DataTxtResponse()
            {
                codigo = 0,
                mensaje = "Datos NO obtenidos",
                rows = new List<DataTxtRowResponse>()
            };
            try
            {
                log.Info("Lectura de csv. Fecha: " + DateTime.Now.ToString());

                var path = @pathFile; 
                using (TextFieldParser csvParser = new TextFieldParser(path))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { ";" });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    csvParser.ReadLine();

                    int row = 1;
                    while (!csvParser.EndOfData)
                    {
                        string[] fields = csvParser.ReadFields();
                        /*string[] fila = csvParser.ReadFields();
                        string[] fields = fila[0].Split(',');*/

                        DataTxtRowResponse dataRow = new DataTxtRowResponse();
                        dataRow.numFila = row;
                        
                        dataRow.numeroObligacion = fields[0];
                        dataRow.usuario = fields[1];
                        dataRow.hora = fields[2];
                        dataRow.fecha = fields[3];
                        dataRow.numeroDocumento = fields[4];
                        dataRow.nombreCompleto = fields[5];
                        dataRow.telefono = fields[6];
                        dataRow.direccion = fields[7];
                        dataRow.canalContacto = fields[8];
                        dataRow.estadoContacto = fields[9];
                        dataRow.estadoCliente = fields[10];
                        dataRow.guion1 = fields[11];
                        dataRow.guion2 = fields[12];
                        dataRow.guion3 = fields[13];
                        dataRow.guion4 = fields[14];
                        dataRow.guion5 = fields[15];
                        dataRow.guion6 = fields[16];
                        dataRow.guion7 = fields[17];
                        dataRow.guion8 = fields[18];
                        dataRow.guion9 = fields[19];
                        dataRow.guion10 = fields[20];
                        dataRow.zona = fields[21];
                        dataRow.actividad = fields[22];
                        dataRow.fechaActividad = fields[23];
                        dataRow.horaActividad = fields[24];
                        dataRow.observacion = fields[25];
                        dataRow.fechaCompromiso = fields[26];
                        dataRow.valorCompromiso = fields[27];
                        dataRow.ciclo = fields[28];
                        dataRow.diasMora = fields[29];
                        dataRow.moraActual = fields[30];
                        dataRow.llaveEspecial = fields[31];
                        dataRow.marca = fields[32];
                        dataRow.marca2 = fields[33];

                        response.rows.Add(dataRow);
                        row++;
                        log.Info("Recorrido de datos de excel. Cantidad registros: " + response.rows.Count() + ". Fecha: " + DateTime.Now.ToString());
                    }
                }
                response.totalRows = response.rows.Count();
                response.codigo = 1;
                response.mensaje = "Datos obtenidos correctamente.";
            }
            catch (Exception ex)
            {
                response.codigo = -1;
                response.mensaje = "Error interno al leer datos del csv.";
                response.error = ex.Message;
                log.Error("Error interno al leer datos del excel. Fecha: " + DateTime.Now.ToString());
                log.Error("Error detallado: " + ex.ToString() + " Fecha: " + DateTime.Now.ToString());
            }
            return response;
        }
    }
}
