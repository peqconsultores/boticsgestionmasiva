﻿using botICSGestionMasivo.Helpers;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Threading;

namespace botICSGestionMasivo.Proceso
{
    class DescargaTxt
    {
        private static ILog log = LogManager.GetLogger(typeof(DescargaTxt));
        private string urlPage = ConfigurationManager.AppSettings["iAgree_url"];
        private string urlPageLogged = ConfigurationManager.AppSettings["iAgreeLogeado_url"]; 

        protected By Options = By.ClassName("profile-name");
        protected By Inicio = By.Id("mainForm:j_idt56");
        protected By Inicio2 = By.Id("mainForm:j_idt55");
        protected By Buscador = By.Id("mainForm:dtGrupoCampanas:nomGrupo");
        protected By Entidad = By.Id("mainForm:dtGrupoCampanas:nomGrupo");
        protected By Detalle = By.Id("mainForm:dtCampanas:0:j_idt198");
        protected By Exportar = By.Id("mainForm:j_idt105");
        protected By SelectTipo = By.Id("mainForm:pgMenuExportacion:tipInfo_label");
        protected By TipoGestionUniverso = By.Id("mainForm:pgMenuExportacion:tipInfo_1");
        protected By InformesCreados = By.Id("mainForm:pgMenuExportacion:filtroColumnas");
        protected By TipoInformesCreados = By.Id("mainForm:pgMenuExportacion:tipInfo_1");
        protected By MejorGestionUniverso = By.Id("mainForm:pgMenuExportacion:filtroColumnas_1");
        protected By fechaDesde = By.Id("mainForm:pgMenuExportacion:fechDesde_input");
        protected By fechaHasta = By.Id("mainForm:pgMenuExportacion:fechasta_input");
        protected By SubirArchivo = By.Id("mainForm:fileUpload_input");
        protected By EnviarArchivo = By.ClassName("ui-button-text");

        protected IWebDriver Driver;
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void keybd_event(uint bVk, uint bScan, uint dwFlags, uint dwExtraInfo);

        [SetUp]
        public void descargarFichero()
        {
            log.Info("Se inicia el navegador. Fecha: " + DateTime.Now.ToString());
            ChromeOptions options = new ChromeOptions();
            Driver = new ChromeDriver(options);
            Driver.Navigate().GoToUrl(urlPage);
            Driver.Manage().Window.Maximize();

            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 5, 0));
            var gestionesEntidad = new List<String>();
            try
            {
                do
                {
                    Thread.Sleep(5000);
                }
                while (Driver.Url != urlPageLogged);
                wait.Until(ExpectedConditions.UrlMatches(urlPageLogged));
                Thread.Sleep(10000);
                wait.Until(ExpectedConditions.ElementExists(Options));
                wait.Until(ExpectedConditions.ElementToBeClickable(Options));
                Driver.FindElement(Options).Click();
                gestionesEntidad=ConvertGestionesDescarga.obtenerGestiones();
            }
            catch(Exception e)
            {
                log.Error("Se ha cerrado el navegador inesperadamente. Fecha: " + DateTime.Now.ToString());
                gestionesEntidad = null;
            }

            bool flagActivo = false;
            foreach (var gestion in gestionesEntidad)
            {
                Driver.Manage().Window.Maximize();
                wait.Until(ExpectedConditions.ElementExists(Inicio));
                wait.Until(ExpectedConditions.ElementToBeClickable(Inicio));
                Driver.FindElement(Inicio).Click();
                wait.Until(ExpectedConditions.ElementExists(Buscador));
                wait.Until(ExpectedConditions.ElementToBeClickable(Buscador));
                Thread.Sleep(3000);
                Driver.FindElement(Buscador).Click();
                Driver.FindElement(Buscador).SendKeys(gestion);
                Thread.Sleep(20000);
                wait.Until(ExpectedConditions.ElementExists(By.XPath("//div[@class='ui-datatable-scrollable-body']/table")));
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//div[@class='ui-datatable-scrollable-body']/table")));
                IWebElement table = Driver.FindElement(By.XPath("//div[@class='ui-datatable-scrollable-body']/table"));
                IWebElement customer = table.FindElement(By.XPath("//tr/td[contains(text(), '" + gestion + "')]"));
                customer.Click();
                wait.Until(ExpectedConditions.ElementExists(Detalle));
                wait.Until(ExpectedConditions.ElementToBeClickable(Detalle));
                Driver.FindElement(Detalle).Click();
                Thread.Sleep(7000);

                for (int c=0;c<10;c++)
                {
                    Thread.Sleep(100);
                    keybd_event(0x09, 0, 0, 0);
                }
                for (int c = 0; c < 10; c++)
                {
                    Thread.Sleep(100);
                    keybd_event(0x28, 0, 0, 0);
                }

                wait.Until(ExpectedConditions.ElementExists(Exportar));
                if (flagActivo == false) {
                    Driver.FindElement(Exportar).FindElement(By.XPath("//a/span[contains(text(), 'Exportar')]")).Click();
                    Thread.Sleep(3000);
                }
                Driver.Manage().Window.FullScreen(); 
                Thread.Sleep(3000);
                Driver.FindElement(Exportar).FindElement(By.XPath("//a/span[contains(text(), 'Exportar')]")).FindElement(By.XPath("//a/span[contains(text(), 'Informes')]")).Click();
                Thread.Sleep(3000);
                Driver.Manage().Window.Maximize();
                wait.Until(ExpectedConditions.ElementExists(SelectTipo));
                wait.Until(ExpectedConditions.ElementToBeClickable(SelectTipo));
                Driver.FindElement(SelectTipo).Click();
                Thread.Sleep(2000);
                wait.Until(ExpectedConditions.ElementExists(TipoGestionUniverso));
                wait.Until(ExpectedConditions.ElementToBeClickable(TipoGestionUniverso));
                Driver.FindElement(TipoGestionUniverso).Click();
                Thread.Sleep(2000);
                if (flagActivo == false)
                {
                    //wait.Until(ExpectedConditions.ElementExists(InformesCreados));
                    //wait.Until(ExpectedConditions.ElementToBeClickable(InformesCreados));
                    //Driver.FindElement(InformesCreados).Click();
                    //Thread.Sleep(2000);
                    //wait.Until(ExpectedConditions.ElementExists(MejorGestionUniverso));
                    //wait.Until(ExpectedConditions.ElementToBeClickable(MejorGestionUniverso));
                    //Driver.FindElement(MejorGestionUniverso).Click();
                    flagActivo = true;
                }
                wait.Until(ExpectedConditions.ElementExists(fechaDesde));
                wait.Until(ExpectedConditions.ElementToBeClickable(fechaDesde));
                Driver.FindElement(fechaDesde).SendKeys(DateTime.Now.AddDays(-1).Day+"/"+ DateTime.Now.AddDays(-1).Month+"/"+ DateTime.Now.AddDays(-1).Year);
                wait.Until(ExpectedConditions.ElementExists(fechaHasta));
                wait.Until(ExpectedConditions.ElementToBeClickable(fechaHasta));
                Driver.FindElement(fechaHasta).SendKeys(DateTime.Now.AddDays(-1).Day + "/" + DateTime.Now.AddDays(-1).Month + "/" + DateTime.Now.AddDays(-1).Year);
                Thread.Sleep(2000);
                wait.Until(ExpectedConditions.ElementExists(By.XPath("//button/span[contains(text(), 'Descargar')]")));
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button/span[contains(text(), 'Descargar')]")));
                Driver.FindElement(By.XPath("//button/span[contains(text(), 'Descargar')]")).Click();
                Thread.Sleep(4000);
                wait.Until(ExpectedConditions.ElementExists(Inicio2));
                wait.Until(ExpectedConditions.ElementToBeClickable(Inicio2));
                Driver.FindElement(Inicio2).Click();

                Thread.Sleep(5000);
            }
            log.Info("Finaliza descarga de archivos txt. Fecha: " + DateTime.Now.ToString());
            Driver.Close();
            Driver.Dispose();
        }
    }
}
