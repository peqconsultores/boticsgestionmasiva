﻿using botICSGestionMasivo.Entities;
using botICSGestionMasivo.Proceso;
using log4net;

namespace botICSGestionMasivo
{
    static class Program
    {
        private static ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            DescargaTxt descargaTxt = new DescargaTxt();
            descargaTxt.descargarFichero();

            HomologacionTxt homologacionTxt = new HomologacionTxt();
            ResponseSimple response = homologacionTxt.ConvertToCsv();

            if (response.codigo == 1)
            {
                ResponseSimple response2 = homologacionTxt.homologarRegistros();

                if (response2.codigo == 1)
                {
                    SubidaExcel subidaExcel = new SubidaExcel();
                    subidaExcel.subidaICS();
                }
            }
        }
    }
}
