﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Constantes
{
    public static class ConstanteGuion3
    {
        public static string PLAN_ALIVIO = "PLAN ALIVIO";
        public static string NO_ESTA_DEACUERDO_CON_EL_VALOR_COBRADO = "NO ESTA DEACUERDO CON EL VALOR COBRADO";
        public static string DESEMPLEO = "DESEMPLEO";
        public static string ESTADO_INACTIVO = "ESTADO INACTIVO";
        public static string POSIBLE_CONDONACION = "POSIBLE CONDONACION";
        public static string SE_EVIDENCIA_PAGO_EN_APLICATIVO = "SE EVIDENCIA PAGO EN APLICATIVO";
        public static string RECORDANDO_PLAN_ALIVIO = "RECORDANDO PLAN ALIVIO";
        public static string SIN_INTERES_DE_PAGO = "SIN INTERES DE PAGO";
        public static string PRESENTA_RECLAMO = "PRESENTA RECLAMO";
        public static string PAGO_POSTERIOR = "PAGO POSTERIOR";
        public static string VOLVER_A_LLAMAR = "VOLVER_A_LLAMAR";
        public static string CLIENTE_AL_DIA = "CLIENTE AL DIA";
        public static string FALLECIDO = "FALLECIDO";
        public static string NO_SE_DEJA_MASAJE = "NO SE DEJA MASAJE";
        public static string CONTESTA_Y_COLGO = "CONTESTA Y COLGO";
        public static string EMPTY = "";
    }
}
