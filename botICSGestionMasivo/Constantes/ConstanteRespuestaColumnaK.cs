﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Constantes
{
    public static class ConstanteRespuestaColumnaK
    {
        public static string PROMISE = "PROMISE";
        public static string RECORDANDO_PAGO = "RECORDANDO PAGO";
        public static string ALTERNATIVA = "ALTERNATIVA";
        public static string YA_PAGO = "YA PAGO";
        public static string RENUENTE = "RENUENTE";
        public static string MESSAGE = "MESSAGE";
        public static string FALLECIDO = "FALLECIDO";
        public static string NO_ANSWER = "NO_ANSWER";
    }
}
