﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Constantes
{
    public static class ConstanteEstadoContacto
    {
        public static string CONTACTO_DIRECTO = "CONTACTO DIRECTO";
        public static string CONTACTO_INDIRECTO = "CONTACTO INDIRECTO";
        public static string SIN_CONTACTO = "SIN CONTACTO";
    }
}
