﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Constantes
{
    public static class ConstanteGuion2
    {
        public static string COMPROMISO = "COMPROMISO";
        public static string COMPROMISO_VIGENTE = "COMPROMISO VIGENTE";
        public static string DESACUERDO = "DESACUERDO";
        public static string DIFICULTAD_DE_PAGO = "DIFICULTAD DE PAGO";
        public static string ESTADO_ICS = "ESTADO ICS";
        public static string OFRECIMIENTO_DE_CONDONACION = "OFRECIMIENTO DE CONDONACION";
        public static string PAGO_EN_APLICATIVO = "PAGO EN APLICATIVO";
        public static string RECORDANDO_PROMESA = "RECORDANDO PROMESA";
        public static string RENUENTE = "RENUENTE";
        public static string SIN_COMPROMISO_DE_PAGO = "SIN COMPROMISO DE PAGO";
        public static string VOLUNTAD_DE_PAGO = "VOLUNTAD DE PAGO";
        public static string VOLVER_A_LLAMAR = "VOLVER A LLAMAR";
        public static string YA_PAGO = "YA PAGO";
        public static string APORTAN_INFORMACION = "APORTAN INFORMACION";
        public static string FALLECIDA = "FALLECIDA";
        public static string NO_SE_DEJA_MENSAJE = "NO SE DEJA MENSAJE";
        public static string SE_DEJA_MENSAJE = "SE DEJA MENSAJE";
        public static string SE_DEJA_MENSAJE_CON_FAMILIAR = "SE DEJA MENSAJE CON FAMILIAR";
        public static string SE_DEJA_MENSAJE_EN_EL_TRABAJO = "SE DEJA MENSAJE EN EL TRABAJO";
        public static string SE_DEJA_MENSAJE_OTRO = "SE DEJA MENSAJE OTRO";
        public static string COLGO = "COLGO";
        public static string CONTESTA_PERO_COLGO = "CONTESTA PERO COLGO";
        public static string FUERA_DE_SERVICIO = "FUERA DE SERVICIO";
        public static string MENSAJE_BUZON = "MENSAJE BUZON";
        public static string MOVIL_APAGADO = "MOVIL APAGADO";
        public static string NO_CONOCIDO = "NO CONOCIDO";
        public static string NO_CONTESTA = "NO CONTESTA";
        public static string NUMERO_ERRADO = "NUMERO ERRADO";
        public static string OCUPADO = "OCUPADO";
        public static string SE_ENVIA_SOLICITUD = "SE ENVIA SOLICITUD";
        public static string EMPTY = "";
    }
}
