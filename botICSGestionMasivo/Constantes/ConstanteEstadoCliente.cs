﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Constantes
{
    public static class ConstanteEstadoCliente
    {
        public static string COMPROMISO_DE_PAGO = "COMPROMISO DE PAGO";
        public static string INSOLVENTE = "INSOLVENTE";
        public static string MENSAJE_CON_TERCERO = "MENSAJE CON TERCERO";
        public static string POSIBLE_ACUERDO = "POSIBLE ACUERDO";
        public static string RENUENTE = "RENUENTE";
        public static string SEGUIMIENTO_Y_O_PRESION = "SEGUIMIENTO Y/O PRESION";
        public static string SIN_CONTACTO = "SIN CONTACTO";
        public static string VOLVER_A_LLAMAR = "VOLVER A LLAMAR";
        public static string YA_PAGO = "YA PAGO";
    }
}
