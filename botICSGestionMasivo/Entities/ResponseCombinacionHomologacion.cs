﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Entities
{
    public class ResponseCombinacionHomologacion
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public string columnaJ { get; set; }
        public string columnaK { get; set; }
        public string columnaM { get; set; }
    }
}
