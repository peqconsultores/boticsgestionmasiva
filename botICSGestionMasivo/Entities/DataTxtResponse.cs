﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace botICSGestionMasivo.Entities
{
    class DataTxtResponse
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public string error { get; set; }
        public int totalRows { get; set; }
        public List<DataTxtRowResponse> rows { get; set; }
    }
    public class DataTxtRowResponse
    {
        public int numFila { get; set; }
        public string numeroObligacion { get; set; }
        public string usuario { get; set; }
        public string hora { get; set; }
        public string fecha { get; set; }
        public string numeroDocumento { get; set; }
        public string nombreCompleto { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string canalContacto { get; set; }
        public string estadoContacto { get; set; }
        public string estadoCliente { get; set; }
        public string guion1 { get; set; }
        public string guion2 { get; set; }
        public string guion3 { get; set; }
        public string guion4 { get; set; }
        public string guion5 { get; set; }
        public string guion6 { get; set; }
        public string guion7 { get; set; }
        public string guion8 { get; set; }
        public string guion9 { get; set; }
        public string guion10 { get; set; }
        public string zona { get; set; }
        public string actividad { get; set; }
        public string fechaActividad { get; set; }
        public string horaActividad { get; set; }
        public string observacion { get; set; }
        public string fechaCompromiso { get; set; }
        public string valorCompromiso { get; set; }
        public string ciclo { get; set; }
        public string diasMora { get; set; }
        public string moraActual { get; set; }
        public string llaveEspecial { get; set; }
        public string marca { get; set; }
        public string marca2 { get; set; }
        public string nivelPrioridadEstadoCliente { get; set; }
        public string nivelPrioridadEstadoContacto { get; set; }
    }
}
